

const { response } = 'express';

const meses = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto', 'Septiembre','Octubre', 'Noviembre','Diciembre'];
const  dias = ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábad'];
const fecha = new Date();
let dia = fecha.getDay()
let mes = fecha.getMonth()
let diaNumber = fecha.getDate()
let anio = fecha.getFullYear()

//hora
let hora = fecha.getHours();
let minutos = fecha.getMinutes()
let segundos = fecha.getSeconds()


const obtenerFechaGet = (req, res = response) => {
  
  res.json({
     
      day  : ` ${ dias[dia] }`,
      fecha_1 : `${ diaNumber } De ${meses[mes]}`,
      year   : fecha.getFullYear(),
      fecha_2 : `${ diaNumber }/${ mes + 1 }/${ anio }`,
      hora   : `${ hora }:${ minutos }:${ segundos }`,
      prefix : `${ hora > 11? 'pm': 'am' }`
      
  })

};



module.exports = {
  obtenerFechaGet,
}