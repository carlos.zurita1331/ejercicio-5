const { Router } = require('express');
const {obtenerFechaGet } = require('../controllers/personaController');

const router = Router();

router.get('/', obtenerFechaGet);

module.exports = router;